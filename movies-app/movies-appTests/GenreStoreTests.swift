//
//  GenreStoreTests.swift
//  movies-appTests
//
//  Created by Mauricio Cardozo de Macedo on 20/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import movies_app

class GenreStoreSpec: QuickSpec {
    override func spec() {

        describe("GenreStore") {

            var mockProvider: MovieProviderMock!
            var fakeStore: GenreStore!
            var mockMovie: Movie = Movie(overview: nil, title: nil, originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: [53, 27]) // unordered ids for: thriller; horror

            beforeEach {
                mockProvider = nil
                fakeStore = nil
            }

            context("when fetching genres for a movie") {
                context("with a successful initialization") {

                    beforeEach {
                        mockProvider = MovieProviderMock(genre: GenreStub.genres!)
                        fakeStore = GenreStore(service: mockProvider)
                    }

                    context("that has multiple genre IDs") {
                        it("should return an array of genres") {
                            let result = fakeStore.genresForMovie(from: mockMovie)!
                            expect(result.count).to(equal(2))
                        }
                    }

                    context("that has no genre ids at all") {
                        beforeEach {
                            mockMovie.genreIDs = nil
                        }
                        it("should return a nil array of genres") {
                            let result = fakeStore.genresForMovie(from: mockMovie)
                            expect(result).to(beNil())
                        }
                    }
                }

                context("with an unsuccessful initialization") {
                    beforeEach {
                        mockProvider = MovieProviderMock(genre: nil)
                        fakeStore = GenreStore(service: mockProvider)
                    }
                    it("should return a nil array of genres for any query") {
                        let result = fakeStore.genresForMovie(from: mockMovie)
                        expect(result).to(beNil())
                    }
                }

            }
        }
    }
}

private struct GenreStub {

    static var genresData: Data {
        return String.data(genreJSON)(using: .utf8, allowLossyConversion: false)!
    }

    static var genres: [Genre]? {
        let decoder = JSONDecoder()
        var genres: [Genre] = []
        do {
            let genreResult: GenreResult = try decoder.decode(GenreResult.self, from: genresData)
            guard genreResult.genres != nil else {
                XCTFail("JSON decode error")
                return nil
            }
            genres = genreResult.genres!
        } catch {
            XCTFail("JSON decode error")
            return nil
        }
        return genres
    }

    private static let genreJSON: String = """
    {
    "genres": [
    {
    "id": 28,
    "name": "Action"
    },
    {
    "id": 12,
    "name": "Adventure"
    },
    {
    "id": 16,
    "name": "Animation"
    },
    {
    "id": 35,
    "name": "Comedy"
    },
    {
    "id": 80,
    "name": "Crime"
    },
    {
    "id": 99,
    "name": "Documentary"
    },
    {
    "id": 18,
    "name": "Drama"
    },
    {
    "id": 10751,
    "name": "Family"
    },
    {
    "id": 14,
    "name": "Fantasy"
    },
    {
    "id": 36,
    "name": "History"
    },
    {
    "id": 27,
    "name": "Horror"
    },
    {
    "id": 10402,
    "name": "Music"
    },
    {
    "id": 9648,
    "name": "Mystery"
    },
    {
    "id": 10749,
    "name": "Romance"
    },
    {
    "id": 878,
    "name": "Science Fiction"
    },
    {
    "id": 10770,
    "name": "TV Movie"
    },
    {
    "id": 53,
    "name": "Thriller"
    },
    {
    "id": 10752,
    "name": "War"
    },
    {
    "id": 37,
    "name": "Western"
    }
    ]
    }
"""
}
