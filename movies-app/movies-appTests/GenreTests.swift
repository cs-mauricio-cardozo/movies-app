//
//  GenreTests.swift
//  movies-appTests
//
//  Created by Luiz Silva on 19/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import XCTest
@testable import movies_app

class GenreTests: XCTestCase {
    
    func testInit() {
        let genre = Genre(id: 10, name: "Adventure")
        
        XCTAssertEqual(genre.id, 10)
        XCTAssertEqual(genre.name, "Adventure")
    }
    
    func testInitFromJson() {
        let json = """
            {
              "id": 10,
              "name": "Adventure",
            }
        """
        
        let data = json.data(using: .utf8)
        XCTAssertNotNil(data)
        
        do {
            let genre = try JSONDecoder().decode(Genre.self, from: data!)
            
            XCTAssertEqual(genre.id, 10)
            XCTAssertEqual(genre.name, "Adventure")
        } catch {
            XCTFail("Failed while decoding json")
        }
    }
    
    func testDecodeNullFields() {
        let json = """
            {
                "id": null,
                "name": null
            }
        """
        
        let data = json.data(using: .utf8)
        XCTAssertNotNil(data)
        
        do {
            let genre = try JSONDecoder().decode(Genre.self, from: data!)
            
            XCTAssertNil(genre.id)
            XCTAssertNil(genre.name)
        } catch {
            XCTFail("Failed while decoding json")
        }
    }
    
    func testDecodeNoFields() {
        let json = "{ }"
        
        let data = json.data(using: .utf8)
        XCTAssertNotNil(data)
        
        do {
            let genre = try JSONDecoder().decode(Genre.self, from: data!)
            
            XCTAssertNil(genre.id)
            XCTAssertNil(genre.name)
        } catch {
            XCTFail("Failed while decoding json")
        }
    }
}
