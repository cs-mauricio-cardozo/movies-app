//
//  MovieListViewControllerTests.swift
//  movies-appTests
//
//  Created by Henrique Santiago Araujo on 20/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import XCTest
@testable import movies_app

class MovieListViewControllerTests: XCTestCase {
    
    var navVC: UINavigationController!
    var vc: MovieListViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
        vc.service = ServiceMock()
        navVC = UINavigationController(rootViewController: vc)
        _ = vc.view
    }
    
    func testLoadingState() {
        vc.state = .loading
        XCTAssertTrue(vc.movieCollectionView.isHidden)
        XCTAssertTrue(vc.errorLabel.isHidden)
        XCTAssertFalse(vc.activityIndicator.isHidden)
        XCTAssertTrue(vc.activityIndicator.isAnimating)
    }
    
    func testErrorState() {
        vc.state = .error
        XCTAssertTrue(vc.movieCollectionView.isHidden)
        XCTAssertTrue(vc.activityIndicator.isHidden)
        XCTAssertFalse(vc.activityIndicator.isAnimating)
        XCTAssertFalse(vc.errorLabel.isHidden)
    }
    
    func testReadyState() {
        XCTAssertFalse(vc.movieCollectionView.isHidden)
        XCTAssertTrue(vc.errorLabel.isHidden)
        XCTAssertTrue(vc.activityIndicator.isHidden)
        XCTAssertFalse(vc.activityIndicator.isAnimating)
    }
    
}

class ServiceMock: MovieProvider {
    
    func searchMovies(query: String, completionHandler: @escaping MovieProvider.Handler) {
        
    }
    
    func fetchGenres(completionHandler: @escaping (Result<[Genre]>) -> Void) {
        
    }
    
    func fetchMovies(from list: MovieListType, completionHandler: @escaping MovieProvider.Handler) {
        let movie1 = Movie(overview: nil, title: "Movie 1", originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        let movie2 = Movie(overview: nil, title: "Movie 2", originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        
        completionHandler(.success([movie1, movie2]))
    }
    
    func fetchPoster(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        
    }
    
    func fetchBackdrop(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        
    }
}
