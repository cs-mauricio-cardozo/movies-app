//
//  MovieCollectionViewCellSpec.swift
//  movies-appTests
//
//  Created by Luiz Silva on 19/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Quick
import Nimble
@testable import movies_app

class MovieCollectionViewCellSpec: QuickSpec {
    
    var sut: MovieCollectionViewCell!
    
    override func spec() {
        describe("MovieCollectionViewCell") {
            
            var sut: MovieCollectionViewCell!
            
            context("when it's configured with a movie") {
                let movie = Movie(overview: "Test", title: "Title", originalTitle: "Título",
                                  posterPath: "http://www.example.com/poster.jpg",
                                  backdropPath: "http://www.example.com/backdrop.jpg",
                                  releaseDate: Date(),
                                  genreIDs: [])
                let image = UIImage()
                let mock = MockMovieProvider(image: image)
                
                beforeEach {
                    let nib = UINib(nibName: "\(MovieCollectionViewCell.self)",
                        bundle: Bundle(for: MovieCollectionViewCell.self))
                    
                    sut = nib.instantiate(withOwner: nil, options: nil)[0] as? MovieCollectionViewCell
                    
                    sut.api = mock
                }
                
                it("should update its movie title label to reflect the movie's title") {
                    sut.configure(with: movie)
                    
                    expect(sut.lblMovieTitle.text).to(equal(movie.title))
                }
                
                it("should call the appropriate API request to change its poster image") {
                    sut.configure(with: movie)
                    
                    expect(mock.requestedMovie).to(equal(movie))
                    expect(sut.imgMoviePoster.image).to(equal(image))
                }
            }
        }
    }
    
    private class MockMovieProvider: MovieProvider {
        var sampleImage: UIImage
        var requestedMovie: Movie?
        
        init(image: UIImage) {
            sampleImage = image
        }
        
        func searchMovies(query: String, completionHandler: @escaping (Result<[Movie]>) -> Void) { }
        
        func fetchMovies(from list: MovieListType, completionHandler: @escaping (Result<[Movie]>) -> Void) { }
        
        func fetchPoster(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
            requestedMovie = movie
            
            completionHandler(.success(sampleImage))
        }
        
        func fetchBackdrop(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) { }
        
        func fetchGenres(completionHandler: @escaping (Result<[Genre]>) -> Void) { }
    }
}
