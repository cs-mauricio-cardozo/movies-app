//
//  URLComposerTests.swift
//  movies-appTests
//
//  Created by Mauricio Cardozo de Macedo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import XCTest
@testable import movies_app

class URLComposerTests: XCTestCase {

    func testComposePopularMovieListURL() {
        //swiftlint:disable:next line_length
        let popularMovieListURLWithFirstPage = "https://api.themoviedb.org/3/movie/popular?api_key=0b6fce48322fc19b858aeb6366fa8a08&language=en-US&page=1"
        let composedURL = URLComposer.listURL(for: .popular)

        guard let url = composedURL else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(url.absoluteString, popularMovieListURLWithFirstPage)
    }

    func testComposePopularMovieListURLWithPageNumber() {
        //swiftlint:disable:next line_length
        let popularMovieListURLWithSecondPage = "https://api.themoviedb.org/3/movie/popular?api_key=0b6fce48322fc19b858aeb6366fa8a08&language=en-US&page=2"
        let composedURL = URLComposer.listURL(for: .popular, page: 2)

        guard let url = composedURL else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(url.absoluteString, popularMovieListURLWithSecondPage)
    }

    func testComposeMovieSearch() {
        //swiftlint:disable:next line_length
        let queryString = "https://api.themoviedb.org/3/search/movie?api_key=0b6fce48322fc19b858aeb6366fa8a08&language=en-US&query=Star%20Wars&page=1&include_adult=false"
        let composedURL = URLComposer.searchURL(for: "Star Wars")

        guard let url = composedURL else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(url.absoluteString, queryString)
    }

    func testComposeMovieSearchWithPageNumber() {
        //swiftlint:disable:next line_length
        let queryString = "https://api.themoviedb.org/3/search/movie?api_key=0b6fce48322fc19b858aeb6366fa8a08&language=en-US&query=Star%20Wars&page=2&include_adult=false"
        let composedURL = URLComposer.searchURL(for: "Star Wars", page: 2)

        guard let url = composedURL else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(url.absoluteString, queryString)
    }

    func testComposeImageURL() {
        let imageString = "https://image.tmdb.org/t/p/w500/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg"
        let composedURL = URLComposer.imageURL(for: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg")

        guard let url = composedURL else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(url.absoluteString, imageString)
    }

    func testComposeGenreURL() {
        let genreURL: String = "https://api.themoviedb.org/3/genre/movie/list?api_key=0b6fce48322fc19b858aeb6366fa8a08&language=en-US"
        guard let composedURL = URLComposer.genreURL() else {
            XCTFail("URL malformed")
            return
        }

        XCTAssertEqual(composedURL.absoluteString, genreURL)
    }

}
