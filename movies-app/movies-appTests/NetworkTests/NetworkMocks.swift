//
//  NetworkMocks.swift
//  movies-appTests
//
//  Created by Mauricio Cardozo de Macedo on 19/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

// swiftlint:disable large_tuple

import Foundation
import UIKit
@testable import movies_app

class URLSessionMock: URLSession {
    let mockInfo: (Data?, URLResponse?, Error?)

    init(data: Data?, response: URLResponse?, error: Error?) {
        self.mockInfo = (data, response, error)
    }

    override func dataTask(with url: URL,
                           completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return URLSessionDataTaskMock(mockInfo: mockInfo, completionHandler: completionHandler)
    }
}

class URLSessionDataTaskMock: URLSessionDataTask {
    let mockInfo: (Data?, URLResponse?, Error?)
    let completionHandler: (Data?, URLResponse?, Error?) -> Void
    init(mockInfo: (Data?, URLResponse?, Error?), completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.mockInfo = mockInfo
        self.completionHandler = completionHandler
    }

    override func resume() {
        completionHandler(mockInfo.0, mockInfo.1, mockInfo.2)
    }
}

class MovieProviderMock: MovieProvider {

    var genre: [Genre]?

    init(genre: [Genre]) {
        self.genre = genre
    }

    init(genre: [Genre]?) {
        self.genre = genre
    }

    func searchMovies(query: String, completionHandler: @escaping MovieProvider.Handler) {
        fatalError("unused")
    }

    func fetchMovies(from list: MovieListType, completionHandler: @escaping MovieProvider.Handler) {
        fatalError("unused")
    }

    func fetchPoster(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        fatalError("unused")
    }

    func fetchBackdrop(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        completionHandler(.success(UIImage()))
    }

    func fetchGenres(completionHandler: @escaping (Result<[Genre]>) -> Void) {
        guard let genres = genre else {
            completionHandler(.error(NetworkError.emptyResults))
            return
        }

        completionHandler(.success(genres))
    }
}
