//
//  MovieTests.swift
//  movies-appTests
//
//  Created by Henrique Santiago Araujo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import XCTest
@testable import movies_app

//swiftlint:disable line_length

class MovieTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInit() {
        let currentDate = Date()
        let movie = Movie(overview: "Overviewtest", title: "Star Wars - The Last Jedi", originalTitle: "Star Wars Episode VIII", posterPath: "/posterPath.jpg", backdropPath: "/backdropPath.jpg", releaseDate: currentDate, genreIDs: [1, 2])

        
        XCTAssertEqual(movie.overview, "Overviewtest")
        XCTAssertEqual(movie.title, "Star Wars - The Last Jedi")
        XCTAssertEqual(movie.originalTitle, "Star Wars Episode VIII")
        XCTAssertEqual(movie.posterPath, "/posterPath.jpg")
        XCTAssertEqual(movie.backdropPath, "/backdropPath.jpg")
        XCTAssertEqual(movie.releaseDate, currentDate)
        
        if let genreIDs = movie.genreIDs {
            XCTAssertEqual(genreIDs, [1, 2])
        } else {
            XCTFail("Genres array is null")
        }
    }

    func testInitFromJson() {
        let json = """
            {
              "poster_path": "/cezWGskPY5x7GaglTTRN4Fugfb8.jpg",
              "adult": false,
              "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
              "genre_ids": [
                878,
                28,
                12
              ],
              "id": 24428,
              "original_title": "The Avengers",
              "original_language": "en",
              "title": "The Avengers",
              "backdrop_path": "/hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg",
              "popularity": 7.353212,
              "vote_count": 8503,
              "video": false,
              "vote_average": 7.33
            }
        """

        let data = json.data(using: .utf8)
        XCTAssertNotNil(data)

        do {
            let movie = try JSONDecoder().decode(Movie.self, from: data!)

            XCTAssertEqual(movie.posterPath, "/cezWGskPY5x7GaglTTRN4Fugfb8.jpg")
            XCTAssertEqual(movie.backdropPath, "/hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg")
            XCTAssertEqual(movie.overview, "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!")
            XCTAssertEqual(movie.originalTitle, "The Avengers")
            XCTAssertEqual(movie.title, "The Avengers")
            XCTAssertNil(movie.releaseDate)
        } catch {
            XCTFail("Failed while decoding json")
        }
    }

    func testDecodeData() {
        let json = """
            {
                "release_date": "2012-04-25",
            }
        """

        let data = json.data(using: .utf8)
        XCTAssertNotNil(data)

        do {
            let decoder = JSONDecoder()
            let movie = try decoder.decode(Movie.self, from: data!)

            var dateComponents = DateComponents()
            dateComponents.year = 2012
            dateComponents.month = 04
            dateComponents.day = 25

            let userCalendar = Calendar.current
            let releaseDate = userCalendar.date(from: dateComponents)

            XCTAssertEqual(movie.releaseDate, releaseDate)
            XCTAssertNil(movie.posterPath)
            XCTAssertNil(movie.backdropPath)
            XCTAssertNil(movie.overview)
            XCTAssertNil(movie.originalTitle)
            XCTAssertNil(movie.title)
            XCTAssertNil(movie.genreIDs)
        } catch {
            XCTFail("Failed while decoding json \(error)")
        }
    }

}
