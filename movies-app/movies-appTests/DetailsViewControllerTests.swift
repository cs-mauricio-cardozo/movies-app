//
//  DetailsViewControllerTests.swift
//  movies-appTests
//
//  Created by Luiz Silva on 18/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import FBSnapshotTestCase
@testable import movies_app

class DetailsViewControllerTests: FBSnapshotTestCase {
    
    override func setUp() {
        super.setUp()
//        self.recordMode = true
    }
    
    func testView() {
        let movie = Movie(overview: nil, title: nil, originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        let serviceMock = MovieProviderMock(genre: nil)
        let store = GenreStore(service: serviceMock)
        let dvc = DetailsViewController(withMovie: movie, genreProvider: store, movieService: serviceMock)
        FBSnapshotVerifyView(dvc.view)
    }
    
}
