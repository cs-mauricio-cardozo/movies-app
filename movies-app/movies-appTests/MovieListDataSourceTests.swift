//
//  MovieListDataSourceTests.swift
//  movies-appTests
//
//  Created by Henrique Santiago Araujo on 19/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import XCTest
@testable import movies_app

class MovieListDataSourceTests: XCTestCase {
    
    var ds: MovieListDataSource!
    let collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: UICollectionViewFlowLayout())
    
    override func setUp() {
        let movie1 = Movie(overview: nil, title: "Movie 1", originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        let movie2 = Movie(overview: nil, title: "Movie 2", originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        
        ds = MovieListDataSource(movies: [movie1, movie2])
        ds.configureCollectionView(collectionView)
    }
    
    func testRegisteredNib() {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ds.reuseID, for: IndexPath(row: 0, section: 0))
        XCTAssert(cell is MovieCollectionViewCell)
    }
    
    func testCount() {
        XCTAssertEqual(ds.movies.count, 2)
        let numberOfItems = ds.collectionView(collectionView, numberOfItemsInSection: 1)
        XCTAssertEqual(numberOfItems, ds.movies.count)
    }
    
    func testCellValues() {
        let ip = IndexPath(row: 0, section: 0)
        guard let cell = ds.collectionView(collectionView, cellForItemAt: ip) as? MovieCollectionViewCell else {
            XCTFail("Failed to get movie cell type")
            return
        }
        
        XCTAssertEqual(cell.lblMovieTitle.text, ds.movies[0].title)
    }
}
