//
//  ViewController.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let movie = Movie(overview: nil, title: "Star Wars", originalTitle: nil, posterPath: nil, backdropPath: nil, releaseDate: nil, genreIDs: nil)
        let controller = DetailsViewController(withMovie: movie)
        present(controller, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
