//
//  GenreProvider.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 20/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

protocol GenreProvider {
    func genresForMovie(from movie: Movie) -> [Genre]?
}
