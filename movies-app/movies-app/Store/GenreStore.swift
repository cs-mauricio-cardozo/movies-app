//
//  GenreStore.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 20/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

class GenreStore: GenreProvider {
    fileprivate var genres: [Genre]?

    init (service: MovieProvider) {
        service.fetchGenres { (result) in
            switch result {
            case .success(let genres):
                self.genres = genres
            default: break
            }
        }
    }

    func genresForMovie(from movie: Movie) -> [Genre]? {
        guard let array = genres?.filter({ (genre) -> Bool in
            guard let id = genre.id, let result = movie.genreIDs?.contains(id) else {
                return false
            }
            return result
        }) else {
            return nil
        }
        if array.isEmpty { return nil }
        return array
    }
}
