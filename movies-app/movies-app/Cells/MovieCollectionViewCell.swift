//
//  MovieCollectionViewCell.swift
//  movies-app
//
//  Created by Luiz Silva on 19/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblMovieTitle: UILabel!
    @IBOutlet weak var imgMoviePoster: UIImageView!
    
    var api: MovieProvider = MovieService(session: URLSession.shared)
    
    private var latestMovie: Movie?
    
    func configure(with movie: Movie) {
        latestMovie = movie
        
        lblMovieTitle.text = movie.title
        
        // NOTE: Work around for the fact that multiple concurrent backdrop requests
        // can be running simultaneously and overriding one another- we save the
        // latest movie loaded within this cell and compare it against the one
        // passed on this `configure(with:)` call to make sure the image we put
        // in is the latest image requested for this cell.
        api.fetchPoster(from: movie) { result in
            guard self.latestMovie == movie else {
                return
            }
            
            switch result {
            case .success(let image):
                self.imgMoviePoster.image = image
            case .error:
                // TODO: Figure out what to do in case of error- right now we
                // just ignore it :c
                break
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.imgMoviePoster.image = nil
    }
}
