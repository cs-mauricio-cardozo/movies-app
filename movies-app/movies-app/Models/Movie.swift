//
//  Movie.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

struct Movie: Decodable, Equatable {

    static func == (lhs: Movie, rhs: Movie) -> Bool {
        return
            lhs.backdropPath == rhs.backdropPath &&
            lhs.originalTitle == rhs.originalTitle &&
            lhs.overview == rhs.overview &&
            lhs.posterPath == rhs.posterPath &&
            lhs.releaseDate == rhs.releaseDate &&
            lhs.title == rhs.title
    }

    var overview: String?
    var title: String?
    var originalTitle: String? //for search purposes
    var posterPath: String?
    var backdropPath: String?
    var releaseDate: Date?
    var genreIDs: [Int]?
    
    enum CodingKeys: String, CodingKey {
        case overview
        case title
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"
        case genreIDs = "genre_ids"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        overview = try container.decodeIfPresent(String.self, forKey: .overview)
        title = try container.decodeIfPresent(String.self, forKey: .title)
        originalTitle = try container.decodeIfPresent(String.self, forKey: .originalTitle)
        posterPath = try container.decodeIfPresent(String.self, forKey: .posterPath)
        backdropPath = try container.decodeIfPresent(String.self, forKey: .backdropPath)

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"

        if let dateStr = try container.decodeIfPresent(String.self, forKey: .releaseDate) {
            releaseDate = formatter.date(from: dateStr)
        }
        
        genreIDs = try container.decodeIfPresent([Int].self, forKey: .genreIDs)
    }
    
    init(overview: String?, title: String?, originalTitle: String?, posterPath: String?, backdropPath: String?, releaseDate: Date?, genreIDs: [Int]?) {
        self.overview = overview
        self.title = title
        self.originalTitle = originalTitle
        self.posterPath = posterPath
        self.backdropPath = backdropPath
        self.releaseDate = releaseDate
        self.genreIDs = genreIDs
    }
}
