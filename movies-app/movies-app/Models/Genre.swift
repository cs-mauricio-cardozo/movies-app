//
//  Genre.swift
//  movies-app
//
//  Created by Luiz Silva on 19/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

struct Genre: Codable, Equatable {
    static func == (lhs: Genre, rhs: Genre) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }

    var id: Int?
    var name: String?
}
