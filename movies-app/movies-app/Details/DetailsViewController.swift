//
//  DetailsViewController.swift
//  movies-app
//
//  Created by Luiz Silva on 18/12/2017.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import EasyPeasy

class DetailsViewController: UIViewController {
    
    let defaultMargin: CGFloat = 22
    
    let imgViewMovieBackdrop = UIImageView()
    
    let lblMovieTitle = DescriptionLabel()
    
    let lblMovieDate = DescriptionLabel()
    
    let lblGenre = DescriptionLabel()

    let txtViewOverview = UITextView()
    
    let movie: Movie

    let provider: GenreProvider

    let service: MovieProvider
    
    init(withMovie movie: Movie, genreProvider: GenreProvider, movieService: MovieProvider) {
        self.movie = movie
        self.provider = genreProvider
        self.service = movieService
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtViewOverview.isEditable = false
        
        //imgViewMovieBackdrop.backgroundColor = UIColor.red
        view.backgroundColor = UIColor.white
        
        bindModelwithViews()
        
        setupHierarchy()
        setupConstraints()
        setupStyle()
    }
    
    func bindModelwithViews() {
        title = movie.title
        lblMovieTitle.text = movie.title
        txtViewOverview.text = movie.overview
        
        if let releaseDate = movie.releaseDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            lblMovieDate.text = formatter.string(from: releaseDate)
        } else {
            lblMovieDate.text = "Release date not found"
        }

        service.fetchBackdrop(from: movie) { (result) in
            switch result {
            case .success(let image):
                self.imgViewMovieBackdrop.image = image
            default:
                break //should use default image
            }
        }

        guard let genres = provider.genresForMovie(from: movie) else {
            lblGenre.text = "Genre not found"
            return
        }

        let genreLabel: String =
            genres.map {$0.name}
            .flatMap {$0}
            .joined(separator: ", ")

        lblGenre.text = genreLabel
    }
    
    func setupStyle() {
        lblMovieTitle.font = UIFont.boldSystemFont(ofSize: 21)
        lblMovieDate.font = UIFont.systemFont(ofSize: 16)

        imgViewMovieBackdrop.contentMode = .scaleAspectFit
        imgViewMovieBackdrop.clipsToBounds = true

        txtViewOverview.textContainerInset = .zero
        txtViewOverview.textContainer.lineFragmentPadding = 0
        txtViewOverview.font = UIFont.systemFont(ofSize: 18)
        txtViewOverview.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    }
    
    func setupHierarchy() {
        view.addSubview(imgViewMovieBackdrop)
        view.addSubview(lblMovieTitle)
        view.addSubview(lblMovieDate)
        view.addSubview(lblGenre)
        view.addSubview(txtViewOverview)
    }
    
    func setupConstraints() {
        imgViewMovieBackdrop.easy.layout(
            Left(defaultMargin),
            Top(0),
            Right(defaultMargin),
            Height(200)
        )
        
        lblMovieTitle.easy.layout(
            Left(defaultMargin),
            Top(defaultMargin).to(imgViewMovieBackdrop),
            Right()
        )
    
        lblMovieDate.easy.layout(
            Left(defaultMargin),
            Top(defaultMargin).to(lblMovieTitle),
            Right()
        )
        
        lblGenre.easy.layout(
            Left(defaultMargin),
            Top(defaultMargin).to(lblMovieDate),
            Right())
        
        txtViewOverview.easy.layout(
            Left(defaultMargin),
            Top(defaultMargin).to(lblGenre),
            Right(defaultMargin),
            Bottom(defaultMargin)
        )
    }
}
