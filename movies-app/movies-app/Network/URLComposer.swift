//
//  URLComposer.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

struct URLComposer {

    fileprivate struct Network {
        static let apiKey: String = "0b6fce48322fc19b858aeb6366fa8a08"
        static let baseURL: String = "https://api.themoviedb.org/3/"
        static let imageURL: String = "https://image.tmdb.org/t/p/w500"
    }

    /// Composes a URL for a specific type of movie list
    static func listURL(for listType: MovieListType, page: Int = 1) -> URL? {
        var components = URLComponents(string: Network.baseURL + "movie/\(listType.rawValue)")

        let queryItems: [URLQueryItem] = [
            URLQueryItem(name: "api_key", value: Network.apiKey),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "page", value: String(page))
        ]

        components?.queryItems = queryItems

        return components?.url
    }

    /// Composes a URL for a movie search query
    static func searchURL(for movie: String, page: Int = 1) -> URL? {
        guard !movie.isEmpty else {
            return nil
        }

        let queryItems: [URLQueryItem] = [
            URLQueryItem(name: "api_key", value: Network.apiKey),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "query", value: movie),
            URLQueryItem(name: "page", value: String(page)),
            URLQueryItem(name: "include_adult", value: String(false))
        ]

        var components = URLComponents(string: Network.baseURL + "search/movie")
        components?.queryItems = queryItems
        return components?.url
    }

    static func imageURL(for image: String) -> URL? {
        let urlString = Network.imageURL + image
        return URL(string: urlString)
    }

    static func genreURL() -> URL? { return URL(string: "\(Network.baseURL)genre/movie/list?api_key=\(Network.apiKey)&language=en-US") }

}
