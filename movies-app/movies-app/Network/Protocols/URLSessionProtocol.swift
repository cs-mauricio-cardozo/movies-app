//
//  URLSessionProtocol.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 19/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

protocol URLSessionProtocol {
    func dataTask(with url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol {}
