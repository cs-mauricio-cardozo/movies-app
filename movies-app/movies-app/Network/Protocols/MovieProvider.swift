//
//  MovieProvider.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol MovieProvider {
    typealias Handler = (Result<[Movie]>) -> Void
    func searchMovies(query: String, completionHandler: @escaping Handler)
    func fetchMovies(from list: MovieListType, completionHandler: @escaping Handler)
    func fetchPoster(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void)
    func fetchBackdrop(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void)
    func fetchGenres(completionHandler: @escaping (Result<[Genre]>) -> Void)
}
