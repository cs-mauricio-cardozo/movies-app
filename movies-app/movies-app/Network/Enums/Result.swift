//
//  Result.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error)
}
