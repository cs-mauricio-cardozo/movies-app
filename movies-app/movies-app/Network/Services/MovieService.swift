//
//  MovieService.swift
//  movies-app
//
//  Created by Mauricio Cardozo de Macedo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import UIKit

class MovieService: MovieProvider {

    fileprivate let decoder: JSONDecoder = JSONDecoder()
    let session: URLSessionProtocol

    init(session: URLSessionProtocol) {
        self.session = session
    }

    func searchMovies(query: String, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        guard let url = URLComposer.searchURL(for: query) else {
            completionHandler(Result.error(NetworkError.malformedURL))
            return
        }

        fetchMovieArray(with: url, completionHandler: completionHandler)
    }

    func fetchMovies(from list: MovieListType, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        guard let url = URLComposer.listURL(for: list) else {
            completionHandler(Result.error(NetworkError.malformedURL))
            return
        }

        fetchMovieArray(with: url, completionHandler: completionHandler)
    }

    fileprivate func fetchMovieArray(with url: URL, completionHandler: @escaping (Result<[Movie]>) -> Void) {
        session.dataTask(with: url) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.connectionError))
                }
                return
            }

            do {
                let movieResult = try self.decoder.decode(MovieResult.self, from: data)

                guard let movies = movieResult.results else {
                    DispatchQueue.main.async {
                        completionHandler(.error(NetworkError.emptyResults))
                    }
                    return
                }

                DispatchQueue.main.async {
                    completionHandler(.success(movies))
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.decodingError))
                }
            }
            }.resume()
    }

    func fetchPoster(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let poster = movie.posterPath else {
            completionHandler(.error(NetworkError.invalidQuery))
            return
        }
        fetchImage(from: poster, completionHandler: completionHandler)
    }

    func fetchBackdrop(from movie: Movie, completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let backdrop = movie.backdropPath else {
            completionHandler(.error(NetworkError.invalidQuery))
            return
        }
        fetchImage(from: backdrop, completionHandler: completionHandler)
    }

    fileprivate func fetchImage(from path: String, completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let url = URLComposer.imageURL(for: path) else {
            completionHandler(.error(NetworkError.malformedURL))
            return
        }

        session.dataTask(with: url) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.connectionError))
                }
                return
            }

            if let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    completionHandler(.success(image))
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.decodingError))
                }
            }
        }.resume()
    }

    func fetchGenres(completionHandler: @escaping (Result<[Genre]>) -> Void) {
        guard let url = URLComposer.genreURL() else {
            completionHandler(.error(NetworkError.malformedURL))
            return
        }

        session.dataTask(with: url) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.connectionError))
                }
                return
            }

            do {
                let genreResult = try self.decoder.decode(GenreResult.self, from: data)

                guard let genres = genreResult.genres else {
                    DispatchQueue.main.async {
                        completionHandler(.error(NetworkError.emptyResults))
                    }
                    return
                }

                DispatchQueue.main.async {
                    completionHandler(.success(genres))
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandler(.error(NetworkError.decodingError))
                }
            }
        }.resume()
    }
}

struct MovieResult: Decodable {
    let results: [Movie]?
}

struct GenreResult: Decodable {
    let genres: [Genre]?
}

enum NetworkError: Error {
    case connectionError
    case malformedURL
    case decodingError
    case emptyResults
    case invalidQuery
}
