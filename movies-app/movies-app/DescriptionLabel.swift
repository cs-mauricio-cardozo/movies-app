//
//  DescriptionLabel.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 19/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import EasyPeasy

class DescriptionLabel: UILabel {
    
    let separator = UIView()
    let separatorHeight: CGFloat = 1
    let padding: CGFloat = 15
    
    override var intrinsicContentSize: CGSize {
        let size = CGSize(width: super.intrinsicContentSize.width, height: super.intrinsicContentSize.height + separatorHeight + padding)
        return size
    }
    
    init() {
        super.init(frame: .zero)
        layoutSeparator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutSeparator()
    }
    
    func layoutSeparator() {
        self.addSubview(separator)
    
        separator.easy.layout(
            Left(),
            Top(-separatorHeight).to(self, ReferenceAttribute.bottom),
            Right(),
            Height(separatorHeight)
        )
        
        separator.backgroundColor = .black
    }

}
