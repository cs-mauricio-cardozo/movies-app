//
//  AppDelegate.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

// swiftlint:disable line_length

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Return if this is a unit test
        if NSClassFromString("XCTest") != nil {
            return true
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateInitialViewController() {
            window?.rootViewController = controller
            window?.makeKeyAndVisible()
        }
        
        return true
    }
}
