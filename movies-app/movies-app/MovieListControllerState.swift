//
//  MovieListControllerState.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 20/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

enum MovieListControllerState {
    case loading
    case error
    case ready([Movie])
}
