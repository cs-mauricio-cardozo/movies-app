//
//  ViewController.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 18/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {
    
    var service: MovieProvider
    var genreStore: GenreProvider
    
    var state = MovieListControllerState.loading {
        didSet {
            switch state {
            case .error:
                configErrorState()
            case .loading:
                configLoadingState()
            case .ready:
                configReadyState()
            }
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var movieCollectionView: UICollectionView!
    @IBOutlet weak var errorLabel: UILabel!
    let dataSource: MovieListDataSource
    
    let searchController = UISearchController(searchResultsController: nil)
    
    required init?(coder aDecoder: NSCoder) {
        dataSource = MovieListDataSource(movies: [])
        service = MovieService(session: URLSession.shared)
        genreStore = GenreStore(service: service)
        super.init(coder: aDecoder)
        dataSource.didSelectMovie = self.presentMovieDetails(_:)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        state = .loading
        style()
        getMoviesFromNetwork()
        self.dataSource.configureCollectionView(self.movieCollectionView)
        
        setupSearchBar()
        setupCollectionView()
    }
    
    private func getMoviesFromNetwork() {
        service.fetchMovies(from: .popular) { (result) in
            switch result {
            case .success(let movies):
                self.state = .ready(movies)
            case .error:
                self.state = .error
            }
        }
    }
    
    private func getMoviesFromNetwork(with query: String) {
        self.state = .loading
        self.service.searchMovies(query: query) { result in
            switch result {
            case .success(let movies):
                self.state = .ready(movies)
            case .error:
                self.state = .error
            }
        }
    }
    
    private func configReadyState() {
        switch state {
        case .ready(let movies):
            errorLabel.isHidden = true
            movieCollectionView.isHidden = false
            
            if dataSource.movies.isEmpty {
                dataSource.movies = movies
            } else {
                dataSource.filteredMovies = movies
            }
        
            movieCollectionView.reloadData()
            stopActivityIndicator()
        default:
            state = .error
        }
    }
    
    private func configLoadingState() {
        movieCollectionView.isHidden = true
        errorLabel.isHidden = true
        activityIndicator.startAnimating()
    }
    
    private func configErrorState() {
        errorLabel.text = "Deu ruim :("
        errorLabel.isHidden = false
        movieCollectionView.isHidden = true
        stopActivityIndicator()
    }
    
    private func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidesWhenStopped = true
    }
    
    private func style() {
        let yellowColor = UIColor(red: 247/255, green: 206/255, blue: 91/255, alpha: 1)
        navigationController?.navigationBar.barTintColor = yellowColor
        
        let purpleColor = UIColor(red: 45/255, green: 48/255, blue: 71/255, alpha: 1)
        searchController.searchBar.tintColor = purpleColor
    }
    
    private func setupSearchBar() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.delegate    = self
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        definesPresentationContext = true
    }
    
    func setupCollectionView() {
        guard let flowLayout = movieCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        let margins: CGFloat = 15
        let width = (view.bounds.width - 3 * margins) / 2
        
        let ratio: CGFloat = 348.0 / 232.0
        
        flowLayout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 15
        flowLayout.itemSize = CGSize(width: width, height: width * ratio)
    }
    
    private func presentMovieDetails(_ movie: Movie) {
        let detailsVC = DetailsViewController(withMovie: movie, genreProvider: genreStore, movieService: service)
        navigationController?.pushViewController(detailsVC, animated: true)
    }
}

extension MovieListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let argument = searchController.searchBar.text else { return }
        self.getMoviesFromNetwork(with: argument)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dataSource.filteredMovies.removeAll()
        self.movieCollectionView.reloadData()
    }
}
