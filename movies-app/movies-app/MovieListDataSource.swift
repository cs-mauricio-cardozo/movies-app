//
//  MovieListDataSource.swift
//  movies-app
//
//  Created by Henrique Santiago Araujo on 19/12/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import UIKit

class MovieListDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let reuseID: String = "MovieCollectionViewCell"
    
    var movies: [Movie]
    var filteredMovies: [Movie]
    
    var didSelectMovie: ((Movie) -> Void)?
    
    init(movies: [Movie]) {
        self.movies = movies
        self.filteredMovies = []
    }
    
    func configureCollectionView(_ collectionView: UICollectionView) {
        let nib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseID)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filteredMovies.isEmpty {
            return movies.count
        } else {
            return filteredMovies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseID, for: indexPath)
        
        guard let movieCell = cell as? MovieCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        var movie: Movie
        
        if filteredMovies.isEmpty {
            movie = movies[indexPath.row]
        } else {
            movie = filteredMovies[indexPath.row]
        }
        
        movieCell.configure(with: movie)
        
        return movieCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var movie: Movie
        
        if filteredMovies.isEmpty {
            movie = movies[indexPath.row]
        } else {
            movie = filteredMovies[indexPath.row]
        }
        
        didSelectMovie?(movie)
    }
    
}
